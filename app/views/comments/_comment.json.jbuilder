json.extract! comment, :id, :comment, :name, :product_id, :created_at, :updated_at
json.url comment_url(comment, format: :json)
